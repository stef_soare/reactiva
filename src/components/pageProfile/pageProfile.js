import React, {Component} from "react";
import CoverImage from './coverImage/coverImage'
import './pageProfile.scss'
class PageProfile extends Component {

  render() {
    return (
      <div className="content-container profile-page">
      <div className='profilePage'>
         <CoverImage className='coverImage'/>
      </div>
    </div>
    )
  }
}

export default PageProfile;
