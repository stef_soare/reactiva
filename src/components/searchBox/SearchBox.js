import React, {Component} from 'react'
import TextField from '@material-ui/core/TextField';
import SearchIcon from '@material-ui/icons/Search';
import IconButton from '@material-ui/core/IconButton';
import FeedStore from '../../stores/feeds';
import './SearchBox.scss'


class SearchBox extends Component {
  constructor(props) {
    super(props)
    this.state = {
      search: '',
      newFilteredFeeds: FeedStore
    }
  }

  updateSearch = e => {
    this.setState({search:e.target.value})
  }

  handleSubmit = e => {
    e.preventDefault()

    const filteredFeeds = FeedStore.filter( feed => {
      return feed.title.toLocaleLowerCase().indexOf(this.state.search.toLocaleLowerCase()) !== -1
    })
    this.setState({newFilteredFeeds: filteredFeeds})
    
  }
  

  render (){

    return (
    <div className='justify-content-center'>
      <form className='search-form' noValidate autoComplete="off" onSubmit={this.handleSubmit}>
        <TextField 
          onChange={this.updateSearch}
          value={this.state.search}
          id="filled-basic" 
          label="Search" 
          variant="filled"
        />
        <IconButton type="submit" aria-label="search">
          <SearchIcon/>
        </IconButton>
      </form>
    </div>
    )
  }
}

export default SearchBox