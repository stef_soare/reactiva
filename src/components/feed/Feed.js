import React, {Component} from 'react';
import Comments from '../feed/comments/Comments'

import LikeButton from '../Buttons/LikeButton';
import HeartButton from '../Buttons/HeartButton';
// import LikeButtonTest from '../Buttons/LikeButtonTest';
import "./Feed.scss";


class Feed extends Component {
  render() {
    return (
      <div className='feed'>

        <button onClick={() => this.props.deleteItem(this.props.keyItem)} className="delete-button">X</button>
        <h4 className='feed-dark'>{this.props.feedToSubmit.title}</h4>
        <p className='feed-dark'>{this.props.feedToSubmit.content}</p>
        <img src={this.props.feedToSubmit.img} alt={this.props.feedToSubmit.title}/>
        <div className='button-container'>
          <LikeButton />
          <HeartButton />
        </div>
        <Comments feedComments={this.props.feedToSubmit.comments} /> {/* Pasam lista de comment-uri a feed-ului - FeedStore */}
        {/* <LikeButtonTest /> */}
      </div>
    );
  }
}

export default Feed;
