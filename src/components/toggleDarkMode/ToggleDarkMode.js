import React, {Component} from 'react';
import './ToggleDarkMode.scss'
import Brightness4Icon from '@material-ui/icons/Brightness4';
import Brightness5Icon from '@material-ui/icons/Brightness5';

class ToggleDarkMode extends Component {

  constructor(props){
    super(props);
    this.state = {
      darkMode: false
    };

  }

  handleChange = () => {
    this.setState({darkMode: !this.state.darkMode});
    if(this.state.darkMode === false){
      document.body.classList.add('dark-mode');
    } else{
      document.body.classList.remove('dark-mode');
    }
  };

  render() {

    return (
      <div className='toggle-dark-mode' onClick={this.handleChange}>
        {this.state.darkMode === false ?
        <div className='dark-mode-icon'>
          <Brightness4Icon style={{ color: '#fff' }} />
          <span className='hint dark'>DARK MODE</span>
        </div> :
        <div className='dark-mode-icon'>
          <Brightness5Icon style={{ color: '#fff' }} />
          <span className='hint light'>LIGHT MODE</span>
        </div>
        }
      {/* <Switch 
        onChange={this.handleChange}
        value="checkedC" 
        inputProps={{ 'aria-label': 'primary checkbox' }}
        color="primary"  
      />
      {
        this.state.darkMode === false ?
          <div className='dark'>DARK MODE</div> :
          <div className='light'>LIGHT MODE</div>
      } */}
      </div>
    )
  }
}

export default ToggleDarkMode