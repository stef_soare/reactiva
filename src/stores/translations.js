/* General Transaltions */
export let generalTranslation = {
  'appTitle': {
    US: 'Reac Class',
    RO: 'Clasa Reactiva',
    FR: 'Classe réactive'
  }
};

/* Header Translations */
export let headerTranslation = {
  'home': {
    US: 'Home',
    RO: 'Acasa',
    FR: 'Maison'
  },

  'newsfeed': {
    US: 'News',
    RO: 'Stiri',
    FR: 'Actualité'
  },

  'watch': {
    US: 'Watch',
    RO: 'Urmărește',
    FR: 'Regarder'
  },

  'marketplace': {
    US: 'Marketplace',
    RO: 'Piata',
    FR: 'Marché'
  },

  'settings': {
    US: 'Settings',
    RO: 'Setări',
    FR: 'Réglages'
  }
};