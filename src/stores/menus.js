let menuStore = [
  {
    code: 'home',
    link: "/"
  },
  {
    code: 'newsfeed',
    link: "/newsfeed"
  },
  {
    code: "watch",
    link: "/watch"
  },
  {
    code: "marketplace",
    link: "/marketplace"
  },
  {
    code: "settings",
    link: "/settings"
  }
];

export default menuStore;
