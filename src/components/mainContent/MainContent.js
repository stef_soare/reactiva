import React, {Component} from 'react';
import FeedStore from '../../stores/feeds';
import Feed from '../feed/Feed';
import NotificationFeed from '../../stores/notificationfeed';
import uuid from 'react-uuid';
import SearchBox from "../searchBox/SearchBox";

import "./MainContent.scss";
import AppsIcon from "@material-ui/icons/Apps";
import ReorderIcon from "@material-ui/icons/Reorder";

class MainContent extends Component {
  constructor(props) {
    super(props);

    // Initializare //
    this.state = {
      me: "Stef Soare",
      inputValue: "",
      feedsList: FeedStore,
      notificationsList: NotificationFeed,
      gridMode: false
    };

    this.postInput = React.createRef();
  }

  // Preia valoarea inputului si il da state-ului.
  handleChange = e => {
    // Asa da. Este reactiv.
    this.setState({
      inputValue: e.target.value
    });

    // ASA NU. NU mai e reactiv
    // this.state.inputValue = "ddd";
  };

  addPost = () => {
    // Preluam valoarea inputului
    console.log(this.state.inputValue);

    // Il pasam catre o variabila locala a functiei
    let newPost = {
      id: uuid(),
      title: '',
      content: this.state.inputValue,
      author: this.state.me,
      img: "https://via.placeholder.com/900x400",
      comments: []
    };
    
    let oldFeedsList = this.state.feedsList;
    oldFeedsList.unshift(newPost);
    

    let newNotifications = this.state.notificationsList;
    newNotifications.unshift(newPost);
   


    // set State cu valoare din input. cream un nou obiect in array-ul feedsList
    this.setState({
      feedsList: oldFeedsList,
      notificationsList: newNotifications
    })
    // console.log("FeedStore:");
    // console.log(this.state.feedsList);
    // console.log("Notifications:");
    // console.log(this.state.notificationsList)
    
  }
  
  deletePost = key => {
    let oldFeedsList = this.state.feedsList;
    oldFeedsList.splice(key, 1);
    this.setState({
      feedsList: oldFeedsList
    })

    let newNotifications = this.state.notificationsList;
    newNotifications.splice(key, 1);
    this.setState({
      notificationsList: newNotifications
    })
  }

  handleGridMode = () => {
    this.setState({ gridMode: !this.state.gridMode });
  };

  render() {
    return (
      <div className="content-container">
        <SearchBox />
        <div className="add-feed mb-4">
          {/* {this.state.inputValue} */}
          <input
            type="text"
            value={this.state.inputValue}
            onChange={this.handleChange}
            className="form-control mb-2"
            ref={this.postInput}
          />
          <button className="btn btn-primary mb-2" onClick={this.addPost}>
            Post
          </button>
          <div className="grid-mode" onClick={this.handleGridMode}>
            {this.state.gridMode === false ? 
              <AppsIcon className='grid-icon' style={{ fontSize: 30 }} /> : 
              <ReorderIcon className='grid-icon' style={{ fontSize: 30 }} />}
          </div>
        </div>

        {/*Listare Feeds*/}
        <div className="container">
          <div className="row">
            {this.state.feedsList.length > 0 && this.state.feedsList.map((feed, index) => {
              const { id } = feed
              return (
                <div className={this.state.gridMode === true ? 'col-md-4' : null} key={index}>
                  <Feed
                    key={id}
                    keyItem={index}
                    deleteItem={this.deletePost}
                    feedToSubmit={feed}
                  />
                </div>
              )  
            }
    )}
          </div>
        </div>
      </div>
    );
  }
}

export default MainContent;
