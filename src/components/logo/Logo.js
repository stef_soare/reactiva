import React, {Component} from "react";
import logo from '../../assets/imgs/logo.svg';

class Logo extends Component {
  render() {
    return (
      <div className="logo-container">
          <img src={logo} className="app-logo" alt="logo" />
      </div>
    )
  }
}

export default Logo;