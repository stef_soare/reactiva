import React, {Component} from 'react';
import Comment from './Comment';
import uuid from 'react-uuid';
import { FaCommentDots } from "react-icons/fa";
import { FaPaperPlane } from "react-icons/fa";


import "./Comments.scss";

class Comments extends Component {
  constructor(props){
    super(props);
    this.state = {
      comments: this.props.feedComments, // populat din proprietatea feed-ului.
      text: '',
      visible: false,
      isDisabled: true
    }

  }

  toggle = () => {
    this.setState(prevState => ({
      visible: !prevState.visible
    }));
  }


  handleChange = (e) => {
    this.setState({
      text: e.target.value
    })     
  }
  

  handleEditChange = (e) => {
    this.setState({
      editedComment: e.target.value
    })     
  }

  
  handleAddComment = () => {
    
    let newComment = {
      id: uuid(),
      author: 'Catalin Stancu',
      created_at: Math.floor(Date.now() / 1000),
      text: this.state.text,
      likesNumber: 0,
      liked: false,
      isDisabled: true
    };

    const updatedComments = [newComment, ...this.state.comments]
    
    this.setState({
      comments: updatedComments,
      text: ''
    })
  };


  handleAddLike = (id) => {

    const selectedComment = this.state.comments.find(comment => comment.id === id)

    !selectedComment.liked ?
    this.setState([
      selectedComment.liked = true,
      selectedComment.likesNumber = selectedComment.likesNumber + 1
    ]) : 
    this.setState([
      selectedComment.liked = false,
      selectedComment.likesNumber = selectedComment.likesNumber - 1
    ])
  }


  handleEdit = (id) => {

    const selectedComment = this.state.comments.find(comment => comment.id === id)
 
      !selectedComment.isDisabled ?
      this.setState(prevState => ([
        selectedComment.isDisabled = prevState.isDisabled
      ]))
      :
      this.setState(prevState => ([
        selectedComment.isDisabled = !prevState.isDisabled,
        selectedComment.editedComment = selectedComment.text
      ]))
  }

  handleSaveEdit = (id) => {

    const selectedComment = this.state.comments.find(comment => comment.id === id)
    this.setState(prevState => ([
      selectedComment.isDisabled = prevState.isDisabled,
      selectedComment.text = this.state.editedComment,
      selectedComment.created_at = Math.floor(Date.now() / 1000)
    ]))

    console.log(selectedComment)
  }


  handleDelete = (id) => {
    
    const newCommentList = this.state.comments.filter(comment => comment.id !== id)
    
    this.setState({
      comments: newCommentList
    })
  }

  render() {
    return (
      <div className="comments-content">
          <div className="p-3 py-4 mb-2 text-center rounded icon-button" onClick={this.toggle}>
            <FaCommentDots />
            {this.state.comments.length > 0 && <span>{this.state.comments.length} Comments</span>}
          </div>
     
        <div className={this.state.visible === true ? 'comments visible' : 'comments'}>
            
            <input
              className="form-control edit-comment-input" 
              name="comment" 
              type="text" 
              value={this.state.text}
              placeholder=""
              onChange={this.handleChange}
            />

          <div className="p-3 py-4 mb-2 text-center rounded icon-button" onClick={this.handleAddComment}>
            <FaPaperPlane />
          </div>

          {this.state.comments.length > 0 && this.state.comments.map((comment, index) => {
            const { author, created_at, text, editedComment, id, liked, likesNumber, isDisabled} = comment;
            return (
                <Comment 
                  key={id}
                  id={id}
                  keyItem={index}
                  author={author}
                  created_at={created_at}
                  text={text}
                  editedComment={editedComment}
                  liked={liked}
                  likesNumber={likesNumber}
                  handleAddLike={this.handleAddLike}
                  handleDelete={this.handleDelete}
                  handleEdit={this.handleEdit}
                  handleEditChange={this.handleEditChange}
                  handleSaveEdit={this.handleSaveEdit}
                  isDisabled={isDisabled}
                />
            )
          })}
        </div>
      </div>
    )
  }
}

export default Comments