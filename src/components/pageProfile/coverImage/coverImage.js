import React, {Component} from "react";
import './coverImage.scss';
import ResizeImage from 'react-resize-image';
import ProfilePicture from '../profilePicture/profilePicture';
import { FaUpload } from "react-icons/fa";

class CoverImage extends Component {
    constructor(props) {
      super(props);
      this.state = {
        file: '',
        imagePreviewUrl: 'https://via.placeholder.com/900x400'
      };
    }
  
    _handleImageChange(e) {
      e.preventDefault();
  
      let reader = new FileReader();
      let file = e.target.files[0];
  
      reader.onloadend = () => {
        this.setState({
          file: file,
          imagePreviewUrl: reader.result
        });
      }
  
      reader.readAsDataURL(file)
    }
  
    render() {
      let {imagePreviewUrl} = this.state;
      let $imagePreview = null;
      if (imagePreviewUrl) {
        $imagePreview = (<ResizeImage src={imagePreviewUrl} />);
      } else {
        $imagePreview = (<div className="previewText">Update Cover</div>);
      }
  
      return (
        <div className="previewComponent">
          <form onSubmit={(e)=>this._handleSubmit(e)} className="upload-cover">
            <label for="upload-image"><FaUpload className='upload-icon' /></label>
            <input className="fileInput" 
              type="file"
              id="upload-image"
              onChange={(e)=>this._handleImageChange(e)} />
          </form>

          <div className="imgPreview">
           {
              $imagePreview = null ? $imagePreview : <img src={this.state.imagePreviewUrl} alt={this.state.imagePreviewUrl} />
             
            }
          </div>
          <div className="picture-profile">
            <ProfilePicture />
          </div>
        </div>
      )
    }
  }
    

export default CoverImage;
