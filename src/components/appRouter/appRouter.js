import React, {Component} from "react";
import { Route, Switch, BrowserRouter } from 'react-router-dom';
import ProfilePage from '../profilePage/profilePage';
import MainContent from '../mainContent/MainContent';

class AppRouter extends Component {

  render() {
    return (
        <BrowserRouter>
        <Switch>
            <Route exact path="/" component={MainContent} />
            <Route exact path="/profile-page" component={ProfilePage} />
        </Switch>
        </BrowserRouter>
    )
  }
}

export default AppRouter;
