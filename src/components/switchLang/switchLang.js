import React, {Component} from 'react';
import ReactFlagsSelect from 'react-flags-select';

//OR import sass module
import 'react-flags-select/scss/react-flags-select.scss';
import './switchLang.scss';


class Languages extends Component{

    render(){
        return(
            <div className='lang-cls float-right'>
              <ReactFlagsSelect
                defaultCountry={this.props.defaultCountry}
                countries={["US", "RO", "FR"]}
                onSelect={this.props.onSelectLang}
                />
            </div>
        )
    }
}

export default Languages