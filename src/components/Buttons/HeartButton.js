import React from 'react';

import './LikeButton.scss';


class HeartButton extends React.Component{

    state = {
        heart: 0
    };

    addHeart = () => {
        let newCount = this.state.heart + 1;
        this.setState({
            heart: newCount
        });
    }

    render(){
        const heart = this.state.heart;

        if (heart === 0) {
            return (
            <div>
                    <div className="like-button" onClick={this.addHeart}>
                        <i className="far fa-heart fa-lg"></i>
                    </div>
            </div>
            );
        }

        if (heart === 1) {
            return (
            <div>
                <div className="like-button" onClick={this.addHeart}>
                    <i className="fas fa-heart fa-lg"></i>
                </div>
            </div>
            );
        }

        if (heart > 1) {
            return (
                <div>
                        <div className="like-button" onClick={this.addHeart}>
                        <i className="fas fa-heart fa-lg"></i>
                            {heart}
                    </div>
                </div>
            );
        }
    }
}

export default HeartButton;