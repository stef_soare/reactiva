import React, {Component} from 'react';
import { Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';
import './UserNav.scss';

class UserNav extends Component {


    constructor(props) {
        super(props);
        this.state = {
            dropdownOpen: false
        }
    }

    toggle() {
        this.setState(prevState => ({
            dropdownOpen: !prevState.dropdownOpen
        }));
    }

    render() {
        return (

            <Dropdown className="dropDown_" isOpen={this.state.dropdownOpen} toggle={this.toggle.bind(this)}>
                <DropdownToggle tag="a" className="nav-link" caret>

                </DropdownToggle>
                <DropdownMenu>
                    <DropdownItem>Manage Page</DropdownItem>
                    <DropdownItem>Manage Groups</DropdownItem>
                    <DropdownItem>Advertising on Facebook</DropdownItem>
                    <DropdownItem>Settings</DropdownItem>
                    <DropdownItem>Log Out</DropdownItem>
                </DropdownMenu>
            </Dropdown>
        )
    }
}

export default UserNav;