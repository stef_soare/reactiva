import React, {Component} from "react";
import './Footer.scss';
import Logo from "../logo/Logo";


class Footer extends Component {
  render() {
    return (
      <footer className="app-footer">
        <div className="container">
          <div className="row">
            <div className="col-4">
              <Logo />
            </div>
            <div className="buttons col-8">
              <nav>
                <ul>
                  <li><a href="/">Privacy</a></li>
                  <li><a href="/">Terms & conditions</a></li>
                  <li><a href="/">Contact</a></li>
                </ul>
              </nav>
            </div>

          </div>
        </div>
      </footer>
    )
  }
}

export default Footer;