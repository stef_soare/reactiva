import React, {Component} from "react";

class LinkMenu extends Component {

  render() {
    return (
      <li>
        <a href={this.props.menuToSubmit.link}>
          {this.props.menuToSubmit.title}
        </a>
      </li>
    )
  }
}

export default LinkMenu;
