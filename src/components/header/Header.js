import React, {Component} from 'react';
import Logo from "../logo/Logo";
import ToggleDarkMode from '../toggleDarkMode/ToggleDarkMode';
import Notifications from '../notifications/Notifications.js';
import './Header.scss';
import Languages from "../switchLang/switchLang";
import { headerTranslation } from "../../stores/translations";
import MenuStore from "../../stores/menus";
import HeadNav from '../headNav/HeadNav';
import UserNav from "../userNav/UserNav";
import { Link } from 'react-router-dom';

class Header extends Component {
  state = {
    countryCode: this.props.defaultCountryCode
  };

  onSelectFlag = (countryCode) => {
    this.setState({countryCode: countryCode});
  };

  render() {
    return (
      <header className="app-header">
        <div className="container">
          <div className="row">
          
            <div className="left-part col-md-2">
            <Link to="/"> <Logo /></Link>
              <ul className='nav-profile'>
                <li>
                <Link to="/profile-page">
                  Profile Page
                  </Link >
                </li>
              </ul>
            </div>
            <div className="search-part col-md-2"> </div>
            <div className="right-part col-md-6">
              <nav>
                <ul>
                  {MenuStore.map((item, index) => {
                    return (
                      <li key={index}>
                        <a href={item.link}>
                          {/* Preia fiecare meniu si apoi preia valoarea dupa cheia la tara */}
                          { headerTranslation[item.code][this.state.countryCode] }
                        </a>
                      </li>
                    )
                   }
                  )}
                  
                  <Notifications />
                  <HeadNav />
                  <UserNav />
              <ToggleDarkMode/>

                </ul>
              </nav>
            </div>
            
            <div className="language-part col-lg-2">
              <Languages onSelectLang={this.onSelectFlag} defaultCountry={this.state.countryCode}/>
            </div>
  
          </div>
        </div>
      </header>
    )
  }
}

export default Header;
