import React, {Component} from 'react';
import Moment from 'react-moment';

import "./Comment.scss";

class Comment extends Component {

  render() {

    const unixTimestamp = this.props.created_at;
    const { author, text, editedComment, id, liked, likesNumber, handleAddLike, handleDelete, handleEdit, handleEditChange, handleSaveEdit, isDisabled } = this.props

    return (
      <div className="comment">
        <div className="author">{author}</div>
        <div className="post-date"><Moment unix format="YYYY-MM-DD HH:mm">{unixTimestamp}</Moment></div>
        <div className="text">
          
          {
            isDisabled ? <span>{text}</span>
            : 
            <div className="edit-comment-input-container">
              <input
                className="form-control edit-comment-input" 
                name="comment" 
                type="text" 
                defaultValue={editedComment}
                placeholder=""
                onChange={handleEditChange}
              />
              <button className="btn btn-primary edit-button" onClick={() => handleSaveEdit(id)}>Save</button>
            </div>
          }
          
        </div>
        <div className="actions">
          <ul>
            <li onClick={() => handleAddLike(id)} className={liked === false ? null : 'liked'}>{likesNumber < 1 ? null : likesNumber} {likesNumber > 1 ? " Likes" : " Like"}</li>
            <li onClick={() => handleEdit(id)}>{isDisabled ? 'Edit' : 'Cancel'}</li>
            <li onClick={() => handleDelete(id)}>Delete</li>
          </ul>
        </div>
      </div>
    )
  }
}

export default Comment