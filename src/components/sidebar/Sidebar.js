import React, {Component} from 'react';
import menuStore from '../../stores/menus';
import LinkMenu from "../linkMenu/LinkMenu";


import './Sidebar.scss';

class Sidebar extends Component {
  render() {
    return (
      <div className="sidebar">
        <nav>
          <ul>
            {menuStore.length > 0 && menuStore.map((menu, index) => {
                return (
                  <div key={index}>
                    <LinkMenu menuToSubmit={menu}/>
                  </div>
                )
              }
            )}
          </ul>
        </nav>
      </div>
      
    )
  }
}

export default Sidebar;
