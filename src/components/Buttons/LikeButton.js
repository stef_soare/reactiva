import React from 'react';

import './LikeButton.scss';


class LikeButton extends React.Component{

    state = {
        likes: 0
    };

    addLike = () => {
        let newCount = this.state.likes + 1;
        this.setState({
            likes: newCount
        });
    }

    render(){
        const likes = this.state.likes;

        if (likes === 0) {
            return (
            <div>
                    <div className="like-button" onClick={this.addLike}>
                        <i className="far fa-thumbs-up fa-lg"></i>
                    </div>
            </div>
            );
        }

        if (likes === 1) {
            return (
            <div>
                    <div className="like-button" onClick={this.addLike}>
                        <i className="fas fa-thumbs-up fa-lg"></i>
                    </div>
            </div>
            );
        }

        if (likes > 1) {
            return (
                <div>

                    <div className="like-button" onClick={this.addLike}>
                        <i className="fas fa-thumbs-up fa-lg"></i>
                            {likes}
                    </div>
                </div>
            );
        }
    }
}

export default LikeButton;