import React, {Component} from "react";
import ResizeImage from 'react-resize-image';
import { FaUpload } from "react-icons/fa";

class ProfilePicture extends Component {
    constructor(props) {
      super(props);
      this.state = {
        file: '',
        imagePreviewUrl: 'https://via.placeholder.com/900x400'
      };
    }
  
    _handleImageChange(e) {
      e.preventDefault();
  
      let reader = new FileReader();
      let file = e.target.files[0];
  
      reader.onloadend = () => {
        this.setState({
          file: file,
          imagePreviewUrl: reader.result
        });
      }
  
      reader.readAsDataURL(file)
    }
  
    render() {
      let {imagePreviewUrl} = this.state;
      let $imagePreview = null;
      if (imagePreviewUrl) {
        $imagePreview = (<ResizeImage src={imagePreviewUrl} />);
      } else {
        $imagePreview = (<div className="previewText">Update Cover</div>);
      }
  
      return (
        <div className="previewComponent">
          <div className="picture-profile">
          <form onSubmit={(e)=>this._handleSubmit(e)} className="upload-profile-picture">
            <label for="profile-image"><FaUpload className='upload-icon' /></label>
            <input className="fileInput" 
              type="file"
              id="profile-image"
              onChange={(e)=>this._handleImageChange(e)} />
          </form>
           {
              $imagePreview = null ? $imagePreview : <img src={this.state.imagePreviewUrl} alt={this.state.imagePreviewUrl} />
             
            }
            {/* <ProfilePicture /> */}
          </div>
        </div>
      )
    }
  }
    

export default ProfilePicture;
