import React, {Component} from 'react';
import Header from "./components/header/Header";
import Footer from "./components/footer/Footer";
import Sidebar from "./components/sidebar/Sidebar";
import PageProfile from './components/pageProfile/pageProfile';
import MainContent from './components/mainContent/MainContent';
import { Route, Switch, BrowserRouter } from 'react-router-dom';



class App extends Component {
  defaultCountryCode = "US";

  render() {
    return (
      
      <div className="App">
        <BrowserRouter>
        <Header defaultCountryCode={this.defaultCountryCode} />
        <main>
          <div className="container">
            <div className="row">
            <Sidebar />
            <Switch>
              <Route exact path="/" component={MainContent} />
              <Route exact path="/profile-page" component={PageProfile} />
            </Switch>
            </div>
          </div>
        </main>
        
        </BrowserRouter>
        <Footer/>
      </div>
    );
  }
}

export default App;
