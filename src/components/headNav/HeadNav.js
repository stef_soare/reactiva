import React, {Component} from 'react';
import friend from '../../assets/imgs/friend.png';
import messenger from '../../assets/imgs/messenger.png';
import './HeadNav.scss';

class HeadNav extends Component {
    render() {
        return (
            <div className="row">
                    <ul className="order-list">
                        <li><img alt="bla" src={friend} className="friend"/> </li>
                        <li><img alt="bla" src={messenger} className="messenger"/> </li>
                    </ul>
            </div>

        )
    }
}

export default HeadNav;