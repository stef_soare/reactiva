import uuid from 'react-uuid'

let FeedStore = [
        {
          id: uuid(),
          title: 'Commodin vivamus maiores',
          content: 'Numquam porta quis pharetra integer porttitor imperdiet, rem facilis. Accusamus? Quisque accusamus, massa metus praesent dicta etiam blandit, rerum laboris, pariatur morbi eiusmod rhoncus! Curae elit phasellus excepteur habitasse consequat! Laoreet proin, nostra eu! Adipisicing.',
          img: 'https://via.placeholder.com/900x400',
          author: 'Petrica Lulea',
          comments: [
            {
              id: uuid(),
              author: 'Bombonel Adrian',
              text: 'Scelerisque hac eros euismod arcu consequatur, tempus egestas pariatur, provident, lacus perspiciatis',
              created_at: '1581629490' /* Timestamp format (unix) */,
              likesNumber: 121,
              liked: false,
              isDisabled: true
            },
            {
              id: uuid(),
              author: 'Stan Valeriu',
              text: 'Facere semper orci mollit fugiat litora voluptatem fames morbi pulvinar sint',
              created_at: '1581706214' /* Timestamp format (unix) */,
              likesNumber: 77,
              liked: false,
              isDisabled: true
            },
            {
              id: uuid(),
              author: 'Neacsu Georgeta',
              text: 'Doloremque perferendis sollicitudin porro ad per adipisicing 🥰 😘',
              created_at: '1581823394' /* Timestamp format (unix) */,
              likesNumber: 316,
              liked: false,
              isDisabled: true
            }
          ]
        },
      
        { 
          id: uuid(),
          title: 'Consequatur dolore duis 2',
          content: 'Consectetuer cupiditate autem varius, perspiciatis esse cubilia iure quam, integer aliqua rerum vel ut facere a? Accusantium asperiores! Egestas sit ultricies ipsum etiam convallis amet faucibus, lacus, senectus dignissimos! Rerum iste consectetur, massa mollis? Parturient.',
          img: 'https://via.placeholder.com/900x400',
          author: "Eustache Traista",
          comments: [
            {
              id: uuid(),
              author: 'Evelina Burlacescu',
              text: ' Sceempus egestas pariatur provident, lacus perspiciatis',
              created_at: '1579982414' /* Timestamp format (unix) */,
              likesNumber: 201,
              liked: false,
              isDisabled: true
            }
          ]
        },
      
        {
          id: uuid(),      
          title: 'Aliquet sem bibi',
          content: 'Augue nunc quidem placeat sem cursus ullam class, imperdiet veniam, rem a diam, iusto facere nemo facilisi ultrices risus irure! Consequatur dolore duis lorem, impedit egestas, habitasse ligula. Etiam corrupti! Aliquet sem. Dictumst nostra, taciti.',
          img: 'https://via.placeholder.com/900x400',
          author: "Valeriu Dinamita",
          comments: []
        }
      ];
      
      export default FeedStore;
      