import React, {Component, Fragment} from 'react';
import './Notifications.scss';
import FeedStore from '../../stores/feeds';
import NotificationFeed from '../../stores/notificationfeed';

class Notifications extends Component {
    constructor(props) {
        super(props);

        this.state = {
            author: "Stef Soare",
            feeds: FeedStore,
            notifications: NotificationFeed,
            length_feed : 0

          };
    }

    delete_notification = (id) => {
    let newList = this.state.notifications;
    newList.splice(id, 1);
    this.setState({
      notifications: newList
    })
    // console.log("FeedStore");
    // console.log(this.state.feeds);
    // console.log("Notifications");
    // console.log(this.state.notifications);
    }

    componentDidMount = () =>
    {
        
        // console.log(FeedStore);
        setInterval(() => {
            // console.log(this.state.feeds);
            // console.log(this.state.length_feed);
            this.setState({
                notifications: NotificationFeed,
                length_feed: Object.keys(NotificationFeed).length
              });
        },100);
          
    }

    
    render(){
        return (
        <div className="dropdown">
            <button className="btn btn-info" >Notifications<span className="icon">{this.state.length_feed}</span></button>
            <div className="dropdown-content">
            {this.state.length_feed > 0 && this.state.notifications.map((feed,index) =>{
                return(
                    <Fragment key={index}>
                <ul className="notifications-list">
                     
                        <li>Author: {this.state.notifications[index].author}</li>
                        <li>Post: {this.state.notifications[index].content}</li>
                        <li><button className="btn btn-danger" onClick = {() => this.delete_notification(index)}>X</button></li>
                </ul>
            
             </Fragment>
                )
            })}
            

            </div>
        </div>
                    )
}

}


export default Notifications;

